/**
 * View Models used by Spring MVC REST controllers.
 */
package pl.kasprowski.web.rest.vm;
